//
//  PersonalData properties.swift
//  Created by -=HIZIR=- on 07/05/2019.
//
import Foundation
import CoreData

extension PersonalData {
	
	@nonobjc public class func fetchRequest() -> NSFetchRequest<PersonalData> {
		return NSFetchRequest<PersonalData>(entityName: "PersonalData")
	}
	
	@NSManaged public var lastUpdate    : NSDate?
	@NSManaged public var name          : String?
	@NSManaged public var number        : String?
	@NSManaged public var password      : String?
	@NSManaged public var passwordIsLock: Bool
	@NSManaged public var recordID      : NSObject?
	@NSManaged public var recordName    : String?
	@NSManaged public var recordType    : String?
	
}
