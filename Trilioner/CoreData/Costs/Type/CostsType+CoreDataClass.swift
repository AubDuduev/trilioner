
import Foundation
import CoreData
import UIKit

@objc(CostsType)
public class CostsType: NSManagedObject {
	
	private let coreData = CoreData()
	
	required convenience init(name: String?, summa: Double = 0.0, icon: UIImage, parentName: String?) {
		self.init(context: PersistentService.context)
		self.name = name
		self.icon = icon
		
    //Получаем основной Расходы и добовляем в негo тип баланс
    self.coreData.fetch(object: CostsMain.self) { [weak self] (сostsMain) in
      guard let self = self else { return }
      if сostsMain == nil  {
        let сostsMain  = CostsMain(context: PersistentService.context)
        сostsMain.name = "CostsMain"
      } else if let сostsMain = сostsMain as? CostsMain {
        сostsMain.addToCostsType(self)
      }
      self.coreData.edit()
    }
	}
}
