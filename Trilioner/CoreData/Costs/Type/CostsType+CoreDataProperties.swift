//
//  CostsType+CoreDataProperties.swift
//  
//
//  Created by -=HIZIR=- on 01/04/2019.
//
//

import Foundation
import CoreData


extension CostsType {
	
	@nonobjc public class func fetchRequest() -> NSFetchRequest<CostsType> {
		return NSFetchRequest<CostsType>(entityName: "CostsType")
	}
	
	@NSManaged public var icon: NSObject?
	@NSManaged public var lastUpdate: Date?
	@NSManaged public var name: String?
	@NSManaged public var recordID: NSObject?
	@NSManaged public var recordName: String?
	@NSManaged public var recordType: String?
	@NSManaged public var summa: Double
	@NSManaged public var costsMain: CostsMain?
	@NSManaged public var operation: NSSet?
	@NSManaged public var podCostsType: NSSet?
	@NSManaged public var parentName: String?
}

// MARK: Generated accessors for operation
extension CostsType {
	
	@objc(addOperationObject:)
	@NSManaged public func addToOperation(_ value: Operation)
	
	@objc(removeOperationObject:)
	@NSManaged public func removeFromOperation(_ value: Operation)
	
	@objc(addOperation:)
	@NSManaged public func addToOperation(_ values: NSSet)
	
	@objc(removeOperation:)
	@NSManaged public func removeFromOperation(_ values: NSSet)
	
}

// MARK: Generated accessors for podCostsType
extension CostsType {
	
	@objc(addPodCostsTypeObject:)
	@NSManaged public func addToPodCostsType(_ value: PodCostsType)
	
	@objc(removePodCostsTypeObject:)
	@NSManaged public func removeFromPodCostsType(_ value: PodCostsType)
	
	@objc(addPodCostsType:)
	@NSManaged public func addToPodCostsType(_ values: NSSet)
	
	@objc(removePodCostsType:)
	@NSManaged public func removeFromPodCostsType(_ values: NSSet)
	
}
