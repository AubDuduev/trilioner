//
//  PodCostsType+CoreDataProperties.swift
//  
//
//  Created by -=HIZIR=- on 01/04/2019.
//
//

import Foundation
import CoreData


extension PodCostsType {
	
	@nonobjc public class func fetchRequest() -> NSFetchRequest<PodCostsType> {
		return NSFetchRequest<PodCostsType>(entityName: "PodCostsType")
	}
	
	@NSManaged public var icon: NSObject?
	@NSManaged public var lastUpdate: Date?
	@NSManaged public var name: String?
	@NSManaged public var recordID: NSObject?
	@NSManaged public var recordName: String?
	@NSManaged public var recordType: String?
	@NSManaged public var summa: Double
	@NSManaged public var costsType: CostsType?
	@NSManaged public var operation: NSSet?
	@NSManaged public var parentName: String?
}

// MARK: Generated accessors for operation
extension PodCostsType {
	
	@objc(addOperationObject:)
	@NSManaged public func addToOperation(_ value: Operation)
	
	@objc(removeOperationObject:)
	@NSManaged public func removeFromOperation(_ value: Operation)
	
	@objc(addOperation:)
	@NSManaged public func addToOperation(_ values: NSSet)
	
	@objc(removeOperation:)
	@NSManaged public func removeFromOperation(_ values: NSSet)
	
}
