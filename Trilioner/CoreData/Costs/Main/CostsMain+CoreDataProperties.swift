//
//  CostsMain+CoreDataProperties.swift
//  MyFinance
//
//  Created by MacBook on 26.08.2018.
//  Copyright © 2018 GurobaDeveloper. All rights reserved.
//
//

import Foundation
import CoreData


extension CostsMain {
	
	@nonobjc public class func fetchRequest() -> NSFetchRequest<CostsMain> {
		return NSFetchRequest<CostsMain>(entityName: "CostsMain")
	}
	
	@NSManaged public var listCostsType: NSObject?
	@NSManaged public var nameCosts: String?
	@NSManaged public var summaTottal: Double
	@NSManaged public var costsType: NSOrderedSet?
	@NSManaged public var name: String?
	
}

// MARK: Generated accessors for costsType
extension CostsMain {

    @objc(insertObject:inCostsTypeAtIndex:)
    @NSManaged public func insertIntoCostsType(_ value: CostsType, at idx: Int)

    @objc(removeObjectFromCostsTypeAtIndex:)
    @NSManaged public func removeFromCostsType(at idx: Int)

    @objc(insertCostsType:atIndexes:)
    @NSManaged public func insertIntoCostsType(_ values: [CostsType], at indexes: NSIndexSet)

    @objc(removeCostsTypeAtIndexes:)
    @NSManaged public func removeFromCostsType(at indexes: NSIndexSet)

    @objc(replaceObjectInCostsTypeAtIndex:withObject:)
    @NSManaged public func replaceCostsType(at idx: Int, with value: CostsType)

    @objc(replaceCostsTypeAtIndexes:withCostsType:)
    @NSManaged public func replaceCostsType(at indexes: NSIndexSet, with values: [CostsType])

    @objc(addCostsTypeObject:)
    @NSManaged public func addToCostsType(_ value: CostsType)

    @objc(removeCostsTypeObject:)
    @NSManaged public func removeFromCostsType(_ value: CostsType)

    @objc(addCostsType:)
    @NSManaged public func addToCostsType(_ values: NSOrderedSet)

    @objc(removeCostsType:)
    @NSManaged public func removeFromCostsType(_ values: NSOrderedSet)

}
