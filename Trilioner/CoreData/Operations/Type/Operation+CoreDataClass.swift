//
//  Operation+CoreDataClass.swift
//  
//
//  Created by -=HIZIR=- on 22/03/2019.
//
//

import Foundation
import CoreData
import UIKit

@objc(Operation)
public class Operation: NSManagedObject {
	
	convenience init(idOperation: IdOperation) {
		self.init(context: PersistentService.context)
		
		self.balanceType  = idOperation.balance
		self.costsType    = idOperation.costs
		self.comment      = idOperation.comment
		self.date         = idOperation.date as NSDate
		
		self.gainCostsIs  = idOperation.gainCostsIs
		self.idOperation  = idOperation.idOperaion
		self.nameBalance  = idOperation.nameBalance
		self.nameType     = idOperation.nameType
		self.photo        = idOperation.photo
		
		self.icon         = idOperation.icon
		self.summa        = idOperation.summa
		self.gainType     = idOperation.gain
		self.podCostsType = idOperation.podCosts
		self.podGainType  = idOperation.podGain
		self.balanceAddIs = idOperation.balanceAddIs
		
		addOperationInCategory()
		addOperationSumm()
	}
	
	private func addOperationInCategory(){
		self.balanceType?.addToOperation(self)
		self.costsType?.addToOperation(self)
		self.podCostsType?.addToOperation(self)
		self.gainType?.addToOperation(self)
		self.podGainType?.addToOperation(self)
	}
	
	private func addOperationSumm(){
		self.costsType?.summa    += self.summa
		self.podCostsType?.summa += self.summa
		self.gainType?.summa     += self.summa
		self.podGainType?.summa  += self.summa
		
		if self.gainCostsIs {
			self.balanceType?.summa += self.summa
		} else {
			self.balanceType?.summa -= self.summa
		}
	}
}

