
import UIKit

class IdOperation {
	
	let tag         : String?
	let comment     : String?
	let date        : Date!
	let summa       : Double!
	let icon        : UIImage!
	let photo       : UIImage?
	var costs       : CostsType?
	var gain        : GainType?
	let nameType    : String?
	var gainCostsIs : Bool!
	var podCosts    : PodCostsType?
	var podGain     : PodGainType?
	
	var nameBalance : String! {
		get {
			return balance.name
		}
	}

	var balanceAddIs = Bool()
	var idOperaion   = String()
	var balance      = BalanceType()

	private func cretaeIdOperation() -> String {
		let dateFormater = DateFormatter()
		dateFormater.dateStyle = .medium
		let dateString = dateFormater.string(from: self.date)
		let type = gainCostsIs ? "Заработал " : "Потратил "  
		let id = type + dateString + " \(summa!)"
	return id
	}
	//balance ID
	init(comment: String?, date: Date, summa: Double, balance: BalanceType, photo: UIImage?, tag: String? ) {
		self.comment      = comment
		self.date         = date
		self.summa        = summa
		self.photo        = photo
		self.tag          = tag
		self.balance      = balance
		self.balanceAddIs = true
		self.icon         = balance.icon as? UIImage
		self.nameType     = balance.name
		self.gainCostsIs  = false
		
		self.idOperaion = cretaeIdOperation()
	}
	//Cost and Gagin ID
	init(comment: String?, date: Date, summa: Double, costs: CostsType?, gain: GainType?, photo: UIImage?, tag: String? ) {
		self.comment  = comment
		self.date     = date
		self.summa    = summa
		self.costs    = costs
		self.gain     = gain
		self.photo    = photo
		self.tag      = tag
		self.costs    = costs
		self.gain     = gain
		
		
		if costs != nil {
			self.icon        = costs?.icon as? UIImage
			self.nameType    = self.costs?.name
			self.gainCostsIs = false
		} else {
			self.icon        = gain?.icon as? UIImage
			self.nameType    = self.gain?.name
			self.gainCostsIs = true
		}
	self.idOperaion = cretaeIdOperation()
	}
	//PodCosts ans PodGagin
	init(comment: String?, date: Date, summa: Double, podCosts: PodCostsType?, podGain: PodGainType?, photo: UIImage?, tag: String?) {
		self.comment  = comment
		self.date     = date
		self.summa    = summa
		self.costs    = podCosts?.costsType
		self.gain     = podGain?.gainType
		self.photo    = photo
		self.tag      = tag
		self.podCosts = podCosts
		self.podGain  = podGain
		self.costs    = podCosts?.costsType
		self.gain     = podGain?.gainType
		
		if podCosts != nil {
			self.icon        = podCosts?.icon as? UIImage
			self.nameType    = podCosts?.name
			self.gainCostsIs = false
		} else {
			self.icon        = podGain?.icon as? UIImage
			self.nameType    = podGain?.name
			self.gainCostsIs = true
		}
	self.idOperaion = cretaeIdOperation()
	}
}
