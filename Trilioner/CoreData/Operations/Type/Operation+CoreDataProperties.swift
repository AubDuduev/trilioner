//
//  Operation+CoreDataProperties.swift
//  
//
//  Created by -=HIZIR=- on 22/03/2019.
//
//

import Foundation
import CoreData


extension Operation {
	
	@nonobjc public class func fetchRequest() -> NSFetchRequest<Operation> {
		return NSFetchRequest<Operation>(entityName: "Operation")
	}
	
	@NSManaged public var comment          : String?
	@NSManaged public var date             : NSDate?
	@NSManaged public var favorites        : Bool
	@NSManaged public var gainCostsIs      : Bool
	@NSManaged public var idOperation      : String?
	@NSManaged public var nameBalance      : String?
	@NSManaged public var nameType         : String?
	@NSManaged public var photo            : NSObject?
	@NSManaged public var icon             : NSObject?
	@NSManaged public var tag              : String?
	@NSManaged public var podGainPodCostsIs: NSObject?
	@NSManaged public var summa			 			 : Double
	@NSManaged public var balanceType			 : BalanceType?
	@NSManaged public var costsType        : CostsType?
	@NSManaged public var gainType         : GainType?
	@NSManaged public var podCostsType     : PodCostsType?
	@NSManaged public var podGainType      : PodGainType?
	@NSManaged public var balanceAddIs     : Bool
}
