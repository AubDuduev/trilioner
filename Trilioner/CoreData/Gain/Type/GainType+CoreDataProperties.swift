//
//  GainType+CoreDataProperties.swift
//  
//
//  Created by -=HIZIR=- on 01/04/2019.
//
//

import Foundation
import CoreData


extension GainType {
	
	@nonobjc public class func fetchRequest() -> NSFetchRequest<GainType> {
		return NSFetchRequest<GainType>(entityName: "GainType")
	}
	
	@NSManaged public var icon: NSObject?
	@NSManaged public var lastUpdate: Date?
	@NSManaged public var name: String?
	@NSManaged public var recordID: NSObject?
	@NSManaged public var recordName: String?
	@NSManaged public var recordType: String?
	@NSManaged public var summa: Double
	@NSManaged public var gainMain: GainMain?
	@NSManaged public var operation: NSSet?
	@NSManaged public var podGainType: NSSet?
	@NSManaged public var parentName: String?
}

// MARK: Generated accessors for operation
extension GainType {
	
	@objc(addOperationObject:)
	@NSManaged public func addToOperation(_ value: Operation)
	
	@objc(removeOperationObject:)
	@NSManaged public func removeFromOperation(_ value: Operation)
	
	@objc(addOperation:)
	@NSManaged public func addToOperation(_ values: NSSet)
	
	@objc(removeOperation:)
	@NSManaged public func removeFromOperation(_ values: NSSet)
	
}

// MARK: Generated accessors for podGainType
extension GainType {
	
	@objc(addPodGainTypeObject:)
	@NSManaged public func addToPodGainType(_ value: PodGainType)
	
	@objc(removePodGainTypeObject:)
	@NSManaged public func removeFromPodGainType(_ value: PodGainType)
	
	@objc(addPodGainType:)
	@NSManaged public func addToPodGainType(_ values: NSSet)
	
	@objc(removePodGainType:)
	@NSManaged public func removeFromPodGainType(_ values: NSSet)
	
}
