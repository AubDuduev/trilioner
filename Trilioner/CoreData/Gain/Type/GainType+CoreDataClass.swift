
import Foundation
import CoreData
import UIKit

@objc(GainType)
public class GainType: NSManagedObject {
  
  private let coreData = CoreData()
  
  required convenience init(name: String?, summa: Double = 0.0, icon: UIImage, parentName: String?) {
    self.init(context: PersistentService.context)
    self.name = name
    self.icon = icon
    
    
    //Получаем основной Доходы и добовляем в негo тип баланс
    self.coreData.fetch(object: GainMain.self) { [weak self] (gainMain) in
      guard let self = self else { return }
      if gainMain == nil  {
        let gainMain  = GainMain(context: PersistentService.context)
        gainMain.name = "GainMain"
      } else if let gainMain = gainMain as? GainMain {
        gainMain.addToGainType(self)
      }
      self.coreData.edit()
    }
  }
}
