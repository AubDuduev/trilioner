//
//  GainMain+CoreDataProperties.swift
//  
//
//  Created by -=HIZIR=- on 22/03/2019.
//
//

import Foundation
import CoreData


extension GainMain {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GainMain> {
        return NSFetchRequest<GainMain>(entityName: "GainMain")
    }

    @NSManaged public var nameGain: String?
    @NSManaged public var summaTottal: Double
    @NSManaged public var gainType: NSOrderedSet?
    @NSManaged public var name: String?
}

// MARK: Generated accessors for gainType
extension GainMain {

    @objc(insertObject:inGainTypeAtIndex:)
    @NSManaged public func insertIntoGainType(_ value: GainType, at idx: Int)

    @objc(removeObjectFromGainTypeAtIndex:)
    @NSManaged public func removeFromGainType(at idx: Int)

    @objc(insertGainType:atIndexes:)
    @NSManaged public func insertIntoGainType(_ values: [GainType], at indexes: NSIndexSet)

    @objc(removeGainTypeAtIndexes:)
    @NSManaged public func removeFromGainType(at indexes: NSIndexSet)

    @objc(replaceObjectInGainTypeAtIndex:withObject:)
    @NSManaged public func replaceGainType(at idx: Int, with value: GainType)

    @objc(replaceGainTypeAtIndexes:withGainType:)
    @NSManaged public func replaceGainType(at indexes: NSIndexSet, with values: [GainType])

    @objc(addGainTypeObject:)
    @NSManaged public func addToGainType(_ value: GainType)

    @objc(removeGainTypeObject:)
    @NSManaged public func removeFromGainType(_ value: GainType)

    @objc(addGainType:)
    @NSManaged public func addToGainType(_ values: NSOrderedSet)

    @objc(removeGainType:)
    @NSManaged public func removeFromGainType(_ values: NSOrderedSet)

}
