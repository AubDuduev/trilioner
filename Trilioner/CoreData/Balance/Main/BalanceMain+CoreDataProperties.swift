
import Foundation
import CoreData


extension BalanceMain {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BalanceMain> {
        return NSFetchRequest<BalanceMain>(entityName: "BalanceMain")
    }

    @NSManaged public var listBalanceType: NSObject?
    @NSManaged public var name: String?
    @NSManaged public var summaTottal: Double
    @NSManaged public var balanceType: NSSet?

}

// MARK: Generated accessors for balanceType
extension BalanceMain {

    @objc(addBalanceTypeObject:)
    @NSManaged public func addToBalanceType(_ value: BalanceType)

    @objc(removeBalanceTypeObject:)
    @NSManaged public func removeFromBalanceType(_ value: BalanceType)

    @objc(addBalanceType:)
    @NSManaged public func addToBalanceType(_ values: NSSet)

    @objc(removeBalanceType:)
    @NSManaged public func removeFromBalanceType(_ values: NSSet)

}
