
import CoreData
import UIKit
import CloudKit

@objc(BalanceType)
public class BalanceType: NSManagedObject, Identifiable {
	
	private let coreData = CoreData()
	
	required convenience init(name: String?, summa: Double = 0.0, icon: UIImage, parentName: String?) {
		self.init(context: PersistentService.context)
		self.name = name
		self.icon = icon
		
		//Получаем основной Баланс и добовляем в негo тип баланс
    self.coreData.fetch(object: BalanceMain.self) { [weak self] (balanceMain) in
      guard let self = self else { return }
			if balanceMain == nil  {
				let newbalanceMain  = BalanceMain(context: PersistentService.context)
				newbalanceMain.name = "BalanceMain"
      } else if let balanceMain = balanceMain as? BalanceMain {
        balanceMain.addToBalanceType(self)
			}
      self.coreData.edit()
		}
	}
}

