
import Foundation
import CoreData
import CloudKit

extension BalanceType {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BalanceType> {
        return NSFetchRequest<BalanceType>(entityName: "BalanceType")
    }

    @NSManaged public var icon       : NSObject?
    @NSManaged public var name       : String?
    @NSManaged public var recordID   : NSObject?
    @NSManaged public var summa      : Double
    @NSManaged public var recordType : String?
		@NSManaged public var recordName : String?
		@NSManaged public var lastUpdate : Date?
    @NSManaged public var balanceMain: BalanceMain?
    @NSManaged public var operation  : NSSet?
	  @NSManaged public var parentName : String?

}

// MARK: Generated accessors for operation
extension BalanceType {

    @objc(addOperationObject:)
    @NSManaged public func addToOperation(_ value: Operation)

    @objc(removeOperationObject:)
    @NSManaged public func removeFromOperation(_ value: Operation)

    @objc(addOperation:)
    @NSManaged public func addToOperation(_ values: NSSet)

    @objc(removeOperation:)
    @NSManaged public func removeFromOperation(_ values: NSSet)

}

