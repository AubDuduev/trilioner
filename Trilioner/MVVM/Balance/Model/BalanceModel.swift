
import UIKit

enum BalanceModel {
	
	case loading
	case getData
	case errorHandler(BalanceViewData?)
	case presentData(BalanceViewData)
	
	
}

