
import Foundation
import Protocols

class BalanceManagers: VMManagers {
  
  let setup    : SetupBalance!
  let server   : ServerBalance!
  let present  : PresentBalance!
  let logic    : LogicBalance!
  let animation: AnimationBalance!
  let router   : RouterBalance!
  
  init(viewModel: BalanceViewModel) {
    
    self.setup     = SetupBalance(viewModel: viewModel)
    self.server    = ServerBalance(viewModel: viewModel)
    self.present   = PresentBalance(viewModel: viewModel)
    self.logic     = LogicBalance(viewModel: viewModel)
    self.animation = AnimationBalance(viewModel: viewModel)
    self.router    = RouterBalance(viewModel: viewModel)
  }
}

