
import UIKit
import SwiftUI
import Combine

struct BalanceView: View {
  
  var viewModel = BalanceViewModel()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: BalanceViewData
  
  var body: some View {
    GeometryReader { frame in
      let size = frame.size
      
      VStack {
        
        Text("\(self.viewData.balances.count)")
        ScrollView(.vertical, showsIndicators: false){
          
          ForEach(Array(self.viewData.balances.enumerated()), id: \.offset){ (index, balance) in
            if index < self.viewData.balances.count / 3 {
              
            HStack(spacing: 0) {
              ForEach(0..<3){_ in
                GDCategoryCell(0, data: GDCategoryData(balance: balance))
                  .frame(width    : size.width / 3,
                         height   : (size.width / 3) + 50,
                         alignment: .center)
              }
            }
            } else if (self.viewData.balances.count - 1) == index {
              //Ячейка добавления категорий
              GDAddCategoryCell()
                .frame(width    : size.width / 3,
                       height   : size.width / 3,
                       alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            }
          }
        }
        .frame(width    : size.width,
               height   : size.height,
               alignment: .center)
      }.background(BackgraundItemView().frame(width    : size.width,
                                              height   : size.height,
                                              alignment: .center))
    }
    .onAppear(){
      self.viewModel.view = self
      self.viewModel.managers.logic.data()
    }
  }
}


struct BalanceViewPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = BalanceViewData()
    BalanceView(viewData: viewData)
  }
}

//struct BalanceData: Identifiable {
//
//  let
//}
//Life cycle


