import UIKit

class BalanceViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: BalanceViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = BalanceViewModel(viewController: self)
  }
}
