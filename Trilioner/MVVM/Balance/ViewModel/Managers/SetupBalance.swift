
import UIKit
import Protocols
import SwiftUI
import SnapKit


class SetupBalance: VMManager {
  
  //MARK: - Public variable
  public var VM: BalanceViewModel!
  
  public func hostingVC(){
//    let viewData = BalanceViewData()
//    let view = BalanceView(viewData: viewData)
//    let hostingVC = UIHostingController(rootView: view)
//    self.VM.VC.view.addSubview(hostingVC.view)
//    self.VM.VC.addChild(hostingVC)
//    hostingVC.view.snp.makeConstraints { (hostingVCView) in
//      hostingVCView.edges.equalTo(self.VM.VC.view)
//    }
  }
  public func cancellableViewData(){
    self.VM.cancellable = self.VM.view.viewData.$balances.sink(receiveValue: { (name) in
      //print(name)
    })
  }
}
//MARK: - Initial
extension SetupBalance {
  
  //MARK: - Inition
  convenience init(viewModel: BalanceViewModel) {
    self.init()
    self.VM = viewModel
  }
}


