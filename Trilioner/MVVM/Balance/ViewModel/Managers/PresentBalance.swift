
import UIKit
import Protocols

class PresentBalance: VMManager {
  
  //MARK: - Public variable
  public var VM: BalanceViewModel!
  
  
}
//MARK: - Initial
extension PresentBalance {
  
  //MARK: - Inition
  convenience init(viewModel: BalanceViewModel) {
    self.init()
    self.VM = viewModel
  }
}

