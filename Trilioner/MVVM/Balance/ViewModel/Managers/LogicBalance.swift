import Foundation
import Protocols

class LogicBalance: VMManager {
  
  //MARK: - Public variable
  public var VM: BalanceViewModel!
  
  public func data(){
    self.VM.balanceModel = .getData
  }
}
//MARK: - Initial
extension LogicBalance {
  
  //MARK: - Inition
  convenience init(viewModel: BalanceViewModel) {
    self.init()
    self.VM = viewModel
  }
}
