import UIKit
import Protocols

class ServerBalance: VMManager {
  
  //MARK: - Public variable
  public var VM: BalanceViewModel!
  
  public func getBalances(){
    DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
   
    self.VM.coreData.fetchArray(object: BalanceType.self) { (balances) in
      self.VM.view.viewData.balances = balances
      self.VM.view.viewData.column   = balances.count / 3 + 1
      self.VM.view.viewData.row      = (balances.isEmpty ? 1 : 3)
      print( self.VM.view.viewData.column)
      print( self.VM.view.viewData.row)
      print("Data get \(balances.count)")
    }
    }
  }
}
//MARK: - Initial
extension ServerBalance {
  
  //MARK: - Inition
  convenience init(viewModel: BalanceViewModel) {
    self.init()
    self.VM = viewModel
  }
}


