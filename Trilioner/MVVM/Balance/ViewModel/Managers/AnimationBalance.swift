import UIKit
import Protocols

class AnimationBalance: VMManager {
  
  //MARK: - Public variable
  public var VM: BalanceViewModel!
  
  
}
//MARK: - Initial
extension AnimationBalance {
  
  //MARK: - Inition
  convenience init(viewModel: BalanceViewModel) {
    self.init()
    self.VM = viewModel
  }
}

