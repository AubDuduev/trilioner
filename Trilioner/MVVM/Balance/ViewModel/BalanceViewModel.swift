
import Foundation
import Protocols
import SwiftUI
import Combine

class BalanceViewModel: VMManagers {
	
	public var balanceModel: BalanceModel = .loading {
		didSet{
			self.commonLogicBalance()
		}
	}
  
  //MARK: - Public variable
  public var managers    : BalanceManagers!
  public var VC          : BalanceViewController!
  public var cancellable : AnyCancellable!
  public let coreData    = CoreData()
  public var view        : BalanceView!
  
  public func commonLogicBalance(){
    
    switch self.balanceModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        self.managers.server.getBalances()
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  init() {
    self.managers = BalanceManagers(viewModel: self)
  }
}
//MARK: - Initial
extension BalanceViewModel {
  
  convenience init(viewController: BalanceViewController) {
    self.init()
    self.VC       = viewController
    self.managers = BalanceManagers(viewModel: self)
  }
  
}
