
import SwiftUI

struct MainTabBarItem: View {
  
  var category: GDCategory
  
  var body: some View {
    VStack {
      category.icon()
        .resizable()
        .renderingMode(.original)
    }
  }
}
