
import Foundation
import Protocols

class MainTabBarManagers: VMManagers {
  
  let setup    : SetupMainTabBar!
  let server   : ServerMainTabBar!
  let present  : PresentMainTabBar!
  let logic    : LogicMainTabBar!
  let animation: AnimationMainTabBar!
  let router   : RouterMainTabBar!
  
  init(viewModel: MainTabBarViewModel) {
    
    self.setup     = SetupMainTabBar(viewModel: viewModel)
    self.server    = ServerMainTabBar(viewModel: viewModel)
    self.present   = PresentMainTabBar(viewModel: viewModel)
    self.logic     = LogicMainTabBar(viewModel: viewModel)
    self.animation = AnimationMainTabBar(viewModel: viewModel)
    self.router    = RouterMainTabBar(viewModel: viewModel)
  }
}

