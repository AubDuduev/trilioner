
import UIKit

enum MainTabBarModel {
	
	case loading
	case getData
	case errorHandler(MainTabBarViewData?)
	case presentData(MainTabBarViewData)
	
	
}

