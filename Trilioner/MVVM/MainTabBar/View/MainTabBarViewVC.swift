
import UIKit
import SwiftUI

struct MainTabBarViewVC: UIViewControllerRepresentable {
  
  typealias UIViewControllerType = UIViewController
  
  public func makeUIViewController(context: Context) -> UIViewController {
    let MainTabBarVC = MainTabBarViewController()
    return MainTabBarVC
  }
  
  public func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    
  }
}
