
import UIKit
import SwiftUI
import Combine

struct MainTabBarView: View {
  
  var viewModel = MainTabBarViewModel()
  @State var selection = 0
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: MainTabBarViewData
  
  var body: some View {
    NavigationView {
      VStack{
        TabView(selection: $selection){
          
          //Balance view
          let viewData = BalanceViewData()
          BalanceView(viewData: viewData)
            .tabItem {
              MainTabBarItem(category: .balance)
            }.tag(0)
          
          //IncomeView view
          let incomeViewData = IncomeViewData()
          IncomeView(viewData: incomeViewData)
            .tabItem {
              MainTabBarItem(category: .income)
            }.tag(1)
          
          //ConsumptionView view
          let consumptionViewData = ConsumptionViewData()
          ConsumptionView(viewData: consumptionViewData)
            .tabItem {
              MainTabBarItem(category: .consumption)
            }.tag(2)
          
          //TransactionsView view
          let transactionsViewData = TransactionsViewData()
          TransactionsView(viewData: transactionsViewData)
            .tabItem {
              MainTabBarItem(category: .transactions)
            }.tag(3)
        }
        .accentColor(.white)
        
      }.navigationBarTitle(GDCategory.title(self.selection),
                            displayMode: NavigationBarItem.TitleDisplayMode.inline)
      
    }
  }
  init(viewData: MainTabBarViewData) {
    self.viewData = viewData
    UITabBar.appearance().unselectedItemTintColor = UIColor.gray
    UITabBar.appearance().barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
  }
}

struct MainTabBarPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = MainTabBarViewData()
    MainTabBarView(viewData: viewData)
      .previewDevice("iPhone 11 Pro")
  }
}

