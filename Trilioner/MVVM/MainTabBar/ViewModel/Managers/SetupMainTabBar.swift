
import UIKit
import Protocols
import SwiftUI
import SnapKit


class SetupMainTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: MainTabBarViewModel!
  
  public func hostingVC(){
    let viewData = MainTabBarViewData()
    let view = MainTabBarView(viewData: viewData)
    let hostingVC = UIHostingController(rootView: view)
    self.VM.VC.view.addSubview(hostingVC.view)
    self.VM.VC.addChild(hostingVC)
    hostingVC.view.snp.makeConstraints { (hostingVCView) in
      hostingVCView.edges.equalTo(self.VM.VC.view)
    }
  }
  public func cancellableViewData(){
    self.VM.cancellable = self.VM.viewData.$name.sink(receiveValue: { (name) in
      print(name)
    })
  }
}
//MARK: - Initial
extension SetupMainTabBar {
  
  //MARK: - Inition
  convenience init(viewModel: MainTabBarViewModel) {
    self.init()
    self.VM = viewModel
  }
}


