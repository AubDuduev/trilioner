
import Foundation
import Protocols
import SwiftUI
import Combine

class OperationsViewModel: VMManagers {
	
	public var OperationsModel: OperationsModel = .loading {
		didSet{
			self.commonLogicOperations()
		}
	}
  
  //MARK: - Public variable
  public var managers    : OperationsManagers!
  public var VC          : OperationsViewController!
  public var viewData    = OperationsViewData()
  public var view        : OperationsView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicOperations(){
    
    switch self.OperationsModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension OperationsViewModel {
  
  convenience init(viewController: OperationsViewController) {
    self.init()
    self.VC       = viewController
    self.managers = OperationsManagers(viewModel: self)
  }
}
