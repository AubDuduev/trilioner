
import UIKit
import Protocols

class PresentOperations: VMManager {
  
  //MARK: - Public variable
  public var VM: OperationsViewModel!
  
  
}
//MARK: - Initial
extension PresentOperations {
  
  //MARK: - Inition
  convenience init(viewModel: OperationsViewModel) {
    self.init()
    self.VM = viewModel
  }
}

