import UIKit
import Protocols

class AnimationOperations: VMManager {
  
  //MARK: - Public variable
  public var VM: OperationsViewModel!
  
  
}
//MARK: - Initial
extension AnimationOperations {
  
  //MARK: - Inition
  convenience init(viewModel: OperationsViewModel) {
    self.init()
    self.VM = viewModel
  }
}

