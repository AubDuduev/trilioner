import Foundation
import Protocols

class LogicOperations: VMManager {
  
  //MARK: - Public variable
  public var VM: OperationsViewModel!
  
  
}
//MARK: - Initial
extension LogicOperations {
  
  //MARK: - Inition
  convenience init(viewModel: OperationsViewModel) {
    self.init()
    self.VM = viewModel
  }
}
