import UIKit

class OperationsViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: OperationsViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = OperationsViewModel(viewController: self)
  }
}
