
import UIKit
import SwiftUI
import Combine

struct OperationsView: View {
  
  @State private var viewModel = OperationsViewModel()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: OperationsViewData
  
  private let collection = GDCalculation()
  
  
  var body: some View {
    GeometryReader { geo in
      let size = geo.size
      //Common stack
      VStack {
        //1 - Spacer 20
        Spacer().frame(width: size.width, height: 20)
        //2 - Top operation data
        HStack {
          //Icon
          IconView(viewData: viewData.categoryData)
            .frame(width: 120, height: 120, alignment: .leading)
          Spacer()
          //Texts
          VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 5) {
            Text("Сбербанк").font(.system(size: 40)).font(.title).foregroundColor(.white)
            Text("500").font(.system(size: 30)).foregroundColor(.white)
            Text(self.viewData.name).font(.system(size: 30)).foregroundColor(.white)
          }
          .padding(.trailing, 30)
          .frame(width: .infinity, height: 120, alignment: .center)
        }.frame(width: .infinity, height: 120, alignment: .leading)
        //3 - Spacer 30
        Spacer().frame(width: size.width, height: 30)
        //4 - Calculation operation
        VStack(alignment: .center, spacing: 0) {
          ForEach(0..<collection.row){ row in
           
            HStack(spacing: 0) {
              ForEach(0..<collection.column){ column in
                //Create buttons
                let title = collection.data[IndexPath.index(row, column, collection)]
                CalculationButton(title: title, viewData: self.viewData)
              }
                .frame(width: size.width / 4 - 15, height: size.width / 4 - 15, alignment: .center)
                .background(Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1815167358)))
                .clipShape(Circle())
                .padding(6)
                
            }
          }
        }.frame(maxWidth: .infinity)
        //5 - Colcultion button
        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
          Text("Посчитать")
            .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
            .foregroundColor(.white)
        })
        .background(
          RoundedRectangle(cornerRadius: 35)
            .frame(width: 200, height: 70, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .foregroundColor(Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1815167358)))
            
        )
        .frame(width: 200, height: 70, alignment: .center)
        Spacer()
      }
      .padding(.top, 0)
      .background(BackgraundItemView())
      .edgesIgnoringSafeArea(.bottom)
    }
    .onAppear(){
      self.viewModel.cancellable = self.viewData.$name.sink(receiveValue: { (name) in
        print(name)
      })
    }
  }
}

struct OperationsPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = OperationsViewData()
    OperationsView(viewData: viewData)
      .previewDevice("iPhone SE (2nd generation)")
  }
}

