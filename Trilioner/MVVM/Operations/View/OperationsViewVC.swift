
import UIKit
import SwiftUI

struct OperationsViewVC: UIViewControllerRepresentable {
  
  typealias UIViewControllerType = UIViewController
  
  public func makeUIViewController(context: Context) -> UIViewController {
    let OperationsVC = OperationsViewController()
    return OperationsVC
  }
  
  public func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    
  }
}
