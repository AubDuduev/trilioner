
import Foundation
import Protocols

class OperationsManagers: VMManagers {
  
  let setup    : SetupOperations!
  let server   : ServerOperations!
  let present  : PresentOperations!
  let logic    : LogicOperations!
  let animation: AnimationOperations!
  let router   : RouterOperations!
  
  init(viewModel: OperationsViewModel) {
    
    self.setup     = SetupOperations(viewModel: viewModel)
    self.server    = ServerOperations(viewModel: viewModel)
    self.present   = PresentOperations(viewModel: viewModel)
    self.logic     = LogicOperations(viewModel: viewModel)
    self.animation = AnimationOperations(viewModel: viewModel)
    self.router    = RouterOperations(viewModel: viewModel)
  }
}

