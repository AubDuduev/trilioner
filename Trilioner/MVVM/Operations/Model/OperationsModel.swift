
import UIKit

enum OperationsModel {
	
	case loading
	case getData
	case errorHandler(OperationsViewData?)
	case presentData(OperationsViewData)
	
	
}

