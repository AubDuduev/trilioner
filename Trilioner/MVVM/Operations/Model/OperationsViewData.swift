
import UIKit
import Combine

class OperationsViewData: ObservableObject {
  
  @Published var name         = ""
  @Published var categoryData = GDCategoryData()
  @Published var title        : GDCalculation.GDButton!
}

