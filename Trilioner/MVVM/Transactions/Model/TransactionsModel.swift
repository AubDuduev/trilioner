
import UIKit

enum TransactionsModel {
	
	case loading
	case getData
	case errorHandler(TransactionsViewData?)
	case presentData(TransactionsViewData)
	
	
}

