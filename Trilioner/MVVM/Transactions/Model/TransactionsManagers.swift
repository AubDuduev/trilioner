
import Foundation
import Protocols

class TransactionsManagers: VMManagers {
  
  let setup    : SetupTransactions!
  let server   : ServerTransactions!
  let present  : PresentTransactions!
  let logic    : LogicTransactions!
  let animation: AnimationTransactions!
  let router   : RouterTransactions!
  
  init(viewModel: TransactionsViewModel) {
    
    self.setup     = SetupTransactions(viewModel: viewModel)
    self.server    = ServerTransactions(viewModel: viewModel)
    self.present   = PresentTransactions(viewModel: viewModel)
    self.logic     = LogicTransactions(viewModel: viewModel)
    self.animation = AnimationTransactions(viewModel: viewModel)
    self.router    = RouterTransactions(viewModel: viewModel)
  }
}

