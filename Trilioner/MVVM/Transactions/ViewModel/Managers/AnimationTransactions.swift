import UIKit
import Protocols

class AnimationTransactions: VMManager {
  
  //MARK: - Public variable
  public var VM: TransactionsViewModel!
  
  
}
//MARK: - Initial
extension AnimationTransactions {
  
  //MARK: - Inition
  convenience init(viewModel: TransactionsViewModel) {
    self.init()
    self.VM = viewModel
  }
}

