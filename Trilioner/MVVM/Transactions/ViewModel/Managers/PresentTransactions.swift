
import UIKit
import Protocols

class PresentTransactions: VMManager {
  
  //MARK: - Public variable
  public var VM: TransactionsViewModel!
  
  
}
//MARK: - Initial
extension PresentTransactions {
  
  //MARK: - Inition
  convenience init(viewModel: TransactionsViewModel) {
    self.init()
    self.VM = viewModel
  }
}

