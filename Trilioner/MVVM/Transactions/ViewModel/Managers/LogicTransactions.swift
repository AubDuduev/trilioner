import Foundation
import Protocols

class LogicTransactions: VMManager {
  
  //MARK: - Public variable
  public var VM: TransactionsViewModel!
  
  
}
//MARK: - Initial
extension LogicTransactions {
  
  //MARK: - Inition
  convenience init(viewModel: TransactionsViewModel) {
    self.init()
    self.VM = viewModel
  }
}
