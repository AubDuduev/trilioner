
import Foundation
import Protocols
import SwiftUI
import Combine

class TransactionsViewModel: VMManagers {
	
	public var TransactionsModel: TransactionsModel = .loading {
		didSet{
			self.commonLogicTransactions()
		}
	}
  
  //MARK: - Public variable
  public var managers    : TransactionsManagers!
  public var VC          : TransactionsViewController!
  public var viewData    = TransactionsViewData()
  public var view        : TransactionsView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicTransactions(){
    
    switch self.TransactionsModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension TransactionsViewModel {
  
  convenience init(viewController: TransactionsViewController) {
    self.init()
    self.VC       = viewController
    self.managers = TransactionsManagers(viewModel: self)
  }
}
