
import UIKit
import SwiftUI
import Combine

struct TransactionsView: View {
  
  @State private var viewModel = TransactionsViewModel()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: TransactionsViewData
  
  var body: some View {
    
    ScrollView {
      VStack {
        Spacer().frame(height: 10)
        ForEach(0..<9) { index in
          GDTransactionCell()
            .frame(width: .infinity, height: 100)
        }.frame(width: .infinity, height: .infinity)
        Spacer()
      }
    }.background(BackgraundItemView())
    .onAppear(){
      self.viewModel.cancellable = self.viewData.$name.sink(receiveValue: { (name) in
        
      })
    }
  }
}

struct TransactionsPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = TransactionsViewData()
    TransactionsView(viewData: viewData)
  }
}
