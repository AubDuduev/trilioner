import UIKit

class TransactionsViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: TransactionsViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = TransactionsViewModel(viewController: self)
  }
}
