
import UIKit

enum ConsumptionModel {
	
	case loading
	case getData
	case errorHandler(ConsumptionViewData?)
	case presentData(ConsumptionViewData)
	
	
}

