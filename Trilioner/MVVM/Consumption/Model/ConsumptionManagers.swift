
import Foundation
import Protocols

class ConsumptionManagers: VMManagers {
  
  let setup    : SetupConsumption!
  let server   : ServerConsumption!
  let present  : PresentConsumption!
  let logic    : LogicConsumption!
  let animation: AnimationConsumption!
  let router   : RouterConsumption!
  
  init(viewModel: ConsumptionViewModel) {
    
    self.setup     = SetupConsumption(viewModel: viewModel)
    self.server    = ServerConsumption(viewModel: viewModel)
    self.present   = PresentConsumption(viewModel: viewModel)
    self.logic     = LogicConsumption(viewModel: viewModel)
    self.animation = AnimationConsumption(viewModel: viewModel)
    self.router    = RouterConsumption(viewModel: viewModel)
  }
}

