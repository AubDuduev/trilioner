import UIKit

class ConsumptionViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: ConsumptionViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = ConsumptionViewModel(viewController: self)
  }
}
