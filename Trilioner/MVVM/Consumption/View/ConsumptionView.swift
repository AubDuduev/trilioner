
import UIKit
import SwiftUI
import Combine

struct ConsumptionView: View {
  
  @State private var viewModel = ConsumptionViewModel()
  
  private let collection = GDCollectionCategory()
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: ConsumptionViewData
  
  var body: some View {
    GeometryReader { frame in
      
      let size = frame.size
      
      //Collection view
      Group {
        ScrollView(.vertical, showsIndicators: false){
          
          VStack(alignment: .center, spacing: 0) {
            ForEach(0..<collection.row){ row in
              
              HStack(spacing: 0) {
                ForEach(0..<collection.column){ column in
                  
                  //Проверяем последняя ли ячейка
                  if IndexPath.lastRow(row, column, collection){
                    //Ячейка добавления категорий
                    GDAddCategoryCell()
                      .frame(width    : size.width / 3,
                             height   : size.width / 3,
                             alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                  } else {
                    //Ячейка категорий
                    GDCategoryCell(IndexPath.index(row, column, collection),
                                   data: GDCategoryData(category: .consumption))
                      .frame(width    : size.width / 3,
                             height   : (size.width / 3) + 50,
                             alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                  }
                }
              }
            }
          }
        }.background(BackgraundItemView().frame(width    : frame.size.width,
                                                height   : frame.size.height,
                                                alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/))
        
      }.frame(width    : size.width,
              height   : size.height,
              alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
      //Life cycle
    }
    .onAppear(){
      self.viewModel.cancellable = self.viewModel.viewData.$name.sink(receiveValue: { (name) in
        //self.viewModel.viewData.name = name
      })
    }
  }
}

struct ConsumptionPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = ConsumptionViewData()
    ConsumptionView(viewData: viewData)
  }
}
