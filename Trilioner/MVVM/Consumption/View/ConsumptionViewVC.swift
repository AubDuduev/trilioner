
import UIKit
import SwiftUI

struct ConsumptionViewVC: UIViewControllerRepresentable {
  
  typealias UIViewControllerType = UIViewController
  
  public func makeUIViewController(context: Context) -> UIViewController {
    let ConsumptionVC = ConsumptionViewController()
    return ConsumptionVC
  }
  
  public func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    
  }
}
