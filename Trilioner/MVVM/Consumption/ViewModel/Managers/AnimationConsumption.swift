import UIKit
import Protocols

class AnimationConsumption: VMManager {
  
  //MARK: - Public variable
  public var VM: ConsumptionViewModel!
  
  
}
//MARK: - Initial
extension AnimationConsumption {
  
  //MARK: - Inition
  convenience init(viewModel: ConsumptionViewModel) {
    self.init()
    self.VM = viewModel
  }
}

