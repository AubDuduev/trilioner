import UIKit
import Protocols

class ServerConsumption: VMManager {
  
  //MARK: - Public variable
  public var VM: ConsumptionViewModel!
  
  
}
//MARK: - Initial
extension ServerConsumption {
  
  //MARK: - Inition
  convenience init(viewModel: ConsumptionViewModel) {
    self.init()
    self.VM = viewModel
  }
}


