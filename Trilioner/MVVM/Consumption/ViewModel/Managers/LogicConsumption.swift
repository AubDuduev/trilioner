import Foundation
import Protocols

class LogicConsumption: VMManager {
  
  //MARK: - Public variable
  public var VM: ConsumptionViewModel!
  
  
}
//MARK: - Initial
extension LogicConsumption {
  
  //MARK: - Inition
  convenience init(viewModel: ConsumptionViewModel) {
    self.init()
    self.VM = viewModel
  }
}
