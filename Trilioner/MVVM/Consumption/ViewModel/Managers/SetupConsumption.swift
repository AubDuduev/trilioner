
import UIKit
import Protocols
import SwiftUI
import SnapKit


class SetupConsumption: VMManager {
  
  //MARK: - Public variable
  public var VM: ConsumptionViewModel!
  
  public func hostingVC(){
    let view     = ConsumptionView(viewData: self.VM.viewData)
    let hostingVC = UIHostingController(rootView: view)
    self.VM.VC.view.addSubview(hostingVC.view)
    self.VM.VC.addChild(hostingVC)
    hostingVC.view.snp.makeConstraints { (hostingVCView) in
      hostingVCView.edges.equalTo(self.VM.VC.view)
    }
  }
  public func cancellableViewData(){
    self.VM.cancellable = self.VM.viewData.$name.sink(receiveValue: { (name) in
      print(name)
    })
  }
}
//MARK: - Initial
extension SetupConsumption {
  
  //MARK: - Inition
  convenience init(viewModel: ConsumptionViewModel) {
    self.init()
    self.VM = viewModel
  }
}


