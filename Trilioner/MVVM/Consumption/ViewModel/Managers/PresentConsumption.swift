
import UIKit
import Protocols

class PresentConsumption: VMManager {
  
  //MARK: - Public variable
  public var VM: ConsumptionViewModel!
  
  
}
//MARK: - Initial
extension PresentConsumption {
  
  //MARK: - Inition
  convenience init(viewModel: ConsumptionViewModel) {
    self.init()
    self.VM = viewModel
  }
}

