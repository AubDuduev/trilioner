
import Foundation
import Protocols
import SwiftUI
import Combine

class ConsumptionViewModel: VMManagers {
	
	public var ConsumptionModel: ConsumptionModel = .loading {
		didSet{
			self.commonLogicConsumption()
		}
	}
  
  //MARK: - Public variable
  public var managers    : ConsumptionManagers!
  public var VC          : ConsumptionViewController!
  public var viewData    = ConsumptionViewData()
  public var view        : ConsumptionView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicConsumption(){
    
    switch self.ConsumptionModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension ConsumptionViewModel {
  
  convenience init(viewController: ConsumptionViewController) {
    self.init()
    self.VC       = viewController
    self.managers = ConsumptionManagers(viewModel: self)
  }
}
