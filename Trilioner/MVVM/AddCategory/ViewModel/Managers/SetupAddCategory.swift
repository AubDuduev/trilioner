
import UIKit
import Protocols
import SwiftUI
import SnapKit


class SetupAddCategory: VMManager {
  
  //MARK: - Public variable
  public var VM: AddCategoryViewModel!
  
  public func hostingVC(){
    let view     = AddCategoryView(viewData: self.VM.viewData)
    let hostingVC = UIHostingController(rootView: view)
    self.VM.VC.view.addSubview(hostingVC.view)
    self.VM.VC.addChild(hostingVC)
    hostingVC.view.snp.makeConstraints { (hostingVCView) in
      hostingVCView.edges.equalTo(self.VM.VC.view)
    }
  }
  public func cancellableViewData(){
    self.VM.cancellable = self.VM.viewData.$name.sink(receiveValue: { (name) in
      print(name)
    })
  }
}
//MARK: - Initial
extension SetupAddCategory {
  
  //MARK: - Inition
  convenience init(viewModel: AddCategoryViewModel) {
    self.init()
    self.VM = viewModel
  }
}


