import UIKit
import Protocols

class AnimationAddCategory: VMManager {
  
  //MARK: - Public variable
  public var VM: AddCategoryViewModel!
  
  
}
//MARK: - Initial
extension AnimationAddCategory {
  
  //MARK: - Inition
  convenience init(viewModel: AddCategoryViewModel) {
    self.init()
    self.VM = viewModel
  }
}

