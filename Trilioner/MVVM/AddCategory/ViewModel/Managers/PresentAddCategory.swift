
import UIKit
import Protocols

class PresentAddCategory: VMManager {
  
  //MARK: - Public variable
  public var VM: AddCategoryViewModel!
  
  
}
//MARK: - Initial
extension PresentAddCategory {
  
  //MARK: - Inition
  convenience init(viewModel: AddCategoryViewModel) {
    self.init()
    self.VM = viewModel
  }
}

