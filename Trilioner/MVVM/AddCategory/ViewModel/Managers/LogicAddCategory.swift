import Foundation
import Protocols

class LogicAddCategory: VMManager {
  
  //MARK: - Public variable
  public var VM: AddCategoryViewModel!
  
  public func save(){
    let _  = BalanceType(name: self.VM.viewData.name, icon: self.VM.viewData.image, parentName: "")
    self.VM.coreData.edit()
  }
}
//MARK: - Initial
extension LogicAddCategory {
  
  //MARK: - Inition
  convenience init(viewModel: AddCategoryViewModel) {
    self.init()
    self.VM = viewModel
  }
}
