import UIKit
import Protocols

class ServerAddCategory: VMManager {
  
  //MARK: - Public variable
  public var VM: AddCategoryViewModel!
  
  
}
//MARK: - Initial
extension ServerAddCategory {
  
  //MARK: - Inition
  convenience init(viewModel: AddCategoryViewModel) {
    self.init()
    self.VM = viewModel
  }
}


