
import Foundation
import Protocols
import SwiftUI
import Combine

class AddCategoryViewModel: VMManagers {
	
	public var AddCategoryModel: AddCategoryModel = .loading {
		didSet{
			self.commonLogicAddCategory()
		}
	}
  
  //MARK: - Public variable
  public var managers    : AddCategoryManagers!
  public var VC          : AddCategoryViewController!
  public var viewData    = AddCategoryViewData()
  public var view        : AddCategoryView!
  public var cancellable : AnyCancellable!
  public var coreData    = CoreData()
  
  public func commonLogicAddCategory(){
    
    switch self.AddCategoryModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  init() {
    self.managers = AddCategoryManagers(viewModel: self)
  }
}
//MARK: - Initial
extension AddCategoryViewModel {
  
  convenience init(viewController: AddCategoryViewController) {
    self.init()
    self.VC       = viewController
    self.managers = AddCategoryManagers(viewModel: self)
  }
}
