
import Foundation
import Protocols

class AddCategoryManagers: VMManagers {
  
  let setup    : SetupAddCategory!
  let server   : ServerAddCategory!
  let present  : PresentAddCategory!
  let logic    : LogicAddCategory!
  let animation: AnimationAddCategory!
  let router   : RouterAddCategory!
  
  init(viewModel: AddCategoryViewModel) {
    
    self.setup     = SetupAddCategory(viewModel: viewModel)
    self.server    = ServerAddCategory(viewModel: viewModel)
    self.present   = PresentAddCategory(viewModel: viewModel)
    self.logic     = LogicAddCategory(viewModel: viewModel)
    self.animation = AnimationAddCategory(viewModel: viewModel)
    self.router    = RouterAddCategory(viewModel: viewModel)
  }
}

