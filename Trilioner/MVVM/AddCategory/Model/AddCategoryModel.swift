
import UIKit

enum AddCategoryModel {
	
	case loading
	case getData
	case errorHandler(AddCategoryViewData?)
	case presentData(AddCategoryViewData)
	
	
}

