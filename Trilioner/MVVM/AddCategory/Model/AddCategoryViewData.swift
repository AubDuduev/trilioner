
import UIKit
import Combine

class AddCategoryViewData: ObservableObject {
  
  @Published var name  = ""
  @Published var image = #imageLiteral(resourceName: "Баланс.pdf")
}

