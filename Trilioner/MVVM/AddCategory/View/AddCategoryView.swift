
import UIKit
import SwiftUI
import Combine

struct AddCategoryView: View {
  
  @State private var viewModel = AddCategoryViewModel()
  @State private var textEdit  = ""
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: AddCategoryViewData
  
  var body: some View {
    
    GeometryReader { frame in
      let size = frame.size
      VStack {
        Image(uiImage: viewData.image)
          .resizable()
          .frame(width: 150, height: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        TextField("Введите название категории", text: self.$viewData.name)
          .textFieldStyle(RoundedBorderTextFieldStyle())
          .padding(.horizontal)
          .multilineTextAlignment(.center)
        ZStack {
          RoundedRectangle(cornerRadius: 5)
            .stroke(Color.white, style: /*@START_MENU_TOKEN@*/StrokeStyle()/*@END_MENU_TOKEN@*/)
            .padding()
          GDAddCategoryCollection(viewData: self.viewData)
            .padding()
        }
        Rectangle()
          .frame(width: size.width, height: 20)
          .foregroundColor(.clear)
      }
      .background(BackgraundItemView())
      .edgesIgnoringSafeArea(.bottom)
      .frame(width   : size.width,
             height   : size.height,
             alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
    .navigationBarItems(trailing: Button(action: {
      self.viewModel.managers.logic.save()
    }, label: {
      Text("Save")
    }))
    .onAppear(){
      self.viewModel.cancellable = self.viewData.$name.sink(receiveValue: { (name) in
        print(name)
      })
    }
  }
}
struct AddCategoryPreviews: PreviewProvider {
  static var previews: some View {
    let viewData = AddCategoryViewData()
    AddCategoryView(viewData: viewData)
  }
}
