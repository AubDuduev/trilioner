
import UIKit
import SwiftUI

struct AddCategoryViewVC: UIViewControllerRepresentable {
  
  typealias UIViewControllerType = UIViewController
  
  public func makeUIViewController(context: Context) -> UIViewController {
    let AddCategoryVC = AddCategoryViewController()
    return AddCategoryVC
  }
  
  public func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    
  }
}
