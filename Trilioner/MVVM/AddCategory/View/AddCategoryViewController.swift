import UIKit

class AddCategoryViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: AddCategoryViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = AddCategoryViewModel(viewController: self)
  }
}
