
import Foundation
import Protocols

class IncomeManagers: VMManagers {
  
  let setup    : SetupIncome!
  let server   : ServerIncome!
  let present  : PresentIncome!
  let logic    : LogicIncome!
  let animation: AnimationIncome!
  let router   : RouterIncome!
  
  init(viewModel: IncomeViewModel) {
    
    self.setup     = SetupIncome(viewModel: viewModel)
    self.server    = ServerIncome(viewModel: viewModel)
    self.present   = PresentIncome(viewModel: viewModel)
    self.logic     = LogicIncome(viewModel: viewModel)
    self.animation = AnimationIncome(viewModel: viewModel)
    self.router    = RouterIncome(viewModel: viewModel)
  }
}

