
import UIKit

enum IncomeModel {
	
	case loading
	case getData
	case errorHandler(IncomeViewData?)
	case presentData(IncomeViewData)
	
	
}

