import UIKit

class IncomeViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: IncomeViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
   
  }
  private func initViewModel(){
    self.viewModel = IncomeViewModel(viewController: self)
  }
}
