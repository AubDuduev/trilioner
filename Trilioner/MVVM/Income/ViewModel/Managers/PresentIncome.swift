
import UIKit
import Protocols

class PresentIncome: VMManager {
  
  //MARK: - Public variable
  public var VM: IncomeViewModel!
  
  
}
//MARK: - Initial
extension PresentIncome {
  
  //MARK: - Inition
  convenience init(viewModel: IncomeViewModel) {
    self.init()
    self.VM = viewModel
  }
}

