import Foundation
import Protocols

class LogicIncome: VMManager {
  
  //MARK: - Public variable
  public var VM: IncomeViewModel!
  
  
}
//MARK: - Initial
extension LogicIncome {
  
  //MARK: - Inition
  convenience init(viewModel: IncomeViewModel) {
    self.init()
    self.VM = viewModel
  }
}
