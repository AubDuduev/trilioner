
import UIKit
import Protocols
import SwiftUI
import SnapKit


class SetupIncome: VMManager {
  
  //MARK: - Public variable
  public var VM: IncomeViewModel!
  
  public func hostingVC(){
    let viewData = IncomeViewData()
    let view = IncomeView(viewData: viewData)
    let hostingVC = UIHostingController(rootView: view)
    self.VM.VC.view.addSubview(hostingVC.view)
    self.VM.VC.addChild(hostingVC)
    hostingVC.view.snp.makeConstraints { (hostingVCView) in
      hostingVCView.edges.equalTo(self.VM.VC.view)
    }
  }
  public func cancellableViewData(){
    self.VM.cancellable = self.VM.viewData.$name.sink(receiveValue: { (name) in
      print(name)
    })
  }
}
//MARK: - Initial
extension SetupIncome {
  
  //MARK: - Inition
  convenience init(viewModel: IncomeViewModel) {
    self.init()
    self.VM = viewModel
  }
}


