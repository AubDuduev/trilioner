
import Foundation
import Protocols
import SwiftUI
import Combine

class IncomeViewModel: VMManagers {
	
	public var IncomeModel: IncomeModel = .loading {
		didSet{
			self.commonLogicIncome()
		}
	}
  
  //MARK: - Public variable
  public var managers    : IncomeManagers!
  public var VC          : IncomeViewController!
  public var viewData    = IncomeViewData()
  public var view        : IncomeView!
  public var cancellable : AnyCancellable!
  
  public func commonLogicIncome(){
    
    switch self.IncomeModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension IncomeViewModel {
  
  convenience init(viewController: IncomeViewController) {
    self.init()
    self.VC       = viewController
    self.managers = IncomeManagers(viewModel: self)
  }
}
