
import UIKit

struct GDCategoryData {
  
  var image   : UIImage!    = #imageLiteral(resourceName: "Баланс.pdf")
  var name    : String!     = "Авто"
  var summ    : Int!        = 10
  var category: GDCategory! = .balance
  
  init(image: UIImage! = #imageLiteral(resourceName: "Баланс.pdf"), name: String! = "Авто", summ: Int = 10, category: GDCategory = .balance) {
    
    self.image    = image
    self.name     = name
    self.summ     = summ
    self.category = category
    
  }
  init(balance: BalanceType) {
    self.image = balance.icon as? UIImage
    self.name  = balance.name
  }
}
