
import Foundation

struct GDCalculation: Collectionable {
  
 
  var column = 4
  var row    = 4
  let data   = GDButton.allCases
  
  
  enum GDButton: String, CaseIterable, Identifiable {
    
    var id: UUID {
      return UUID()
    }
    
    case plus  = "+"
    case seven = "7"
    case eight = "8"
    case nine  = "9"
    case minus = "-"
    case fore  = "4"
    case five  = "5"
    case six   = "6"
    case split = "/"
    case one   = "1"
    case two   = "2"
    case three = "3"
    case multiply = "*"
    case dot   = "."
    case ac    = "AC"
    case zero  = "0"
  }
}
