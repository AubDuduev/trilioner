
import SwiftUI

enum GDCategory: String, CaseIterable {
  
  case balance      = "Баланс"
  case income       = "Доходы"
  case consumption  = "Расходы"
  case transactions = "Транзакции"
  
  public func fon() -> Image {
  
    switch self {
      case .balance:
        return Image.set(.fonBalance)
      case .income:
        return Image.set(.fonIncome)
      case .consumption:
        return Image.set(.fonConsumption)
      case .transactions:
        return Image.set(.fonTransactions)
    }
  }
  public func icon() -> Image {
  
    switch self {
      case .balance:
        return Image.set(.balanceIcon)
      case .income:
        return Image.set(.incomeIcon)
      case .consumption:
        return Image.set(.consumptionIcon)
      case .transactions:
        return Image.set(.transactionsIcon)
    }
  }
  public func name() -> String {
    return self.rawValue
  }
  static func title(_ index: Int) -> LocalizedStringKey {
   return LocalizedStringKey(GDCategory.allCases[index].rawValue)
  }
}
