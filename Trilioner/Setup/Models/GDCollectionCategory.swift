
import UIKit

struct GDCollectionCategory: Collectionable {
 
  var column = 3
  var row    = 7
  let padding: CGFloat = 20
  let spacing: CGFloat = 20
  //static let width = (UIScreen.main.bounds.width/CGFloat(column))-padding
}

protocol Collectionable {
  
  var column: Int { get set }
  var row   : Int { get set }
}
