
import UIKit
import SwiftUI

class RootVC {
  
  public func setVC(window: UIWindow?){
    let rootViewController = UIStoryboard.createVC(sbID: .Loading)
    window?.rootViewController = rootViewController
    window?.makeKeyAndVisible()
  }
  public func setHostVC(window: UIWindow?){
    let viewData = MainTabBarViewData()
    let rootView = MainTabBarView(viewData: viewData)
    let rootViewController     = UIHostingController(rootView: rootView)
    window?.rootViewController = rootViewController
    window?.makeKeyAndVisible()
  }
  public func setView(scene: UIScene, window: inout UIWindow?) {
    
    let viewData = MainTabBarViewData()
    let rootView = MainTabBarView(viewData: viewData)
    let scene  = UIApplication.shared.connectedScenes.first
    if let windowScene = scene as? UIWindowScene {
      let createWindow = UIWindow(windowScene: windowScene)
      createWindow.rootViewController = UIHostingController(rootView: rootView)
      window = createWindow
      window?.makeKeyAndVisible()
    }
  }
}

