
import SwiftUI
import Foundation

struct GDCategoryCell: View {
  
  public let pagination: Int!
  public let category  : GDCategory!
  public let data      : GDCategoryData!
  
  @State var tap = false
  
  var body: some View {
    GeometryReader{ frame in
      let size = frame.size
      VStack(spacing: 0) {
        //Images
        Group {
          ZStack {
          IconView(viewData: data)
          let data = OperationsViewData()
          NavigationLink("", destination: OperationsView(viewData: data), isActive: $tap)
          }
        }
        .frame(width: size.width, height: size.width, alignment: .center)
        //Informations
        Group {
          VStack {
            //Texts
            Text("\(data.name)")
              .font(.system(size: 15))
              .foregroundColor(.white)
              .frame(width: size.width, height: 25, alignment: .center)
            Text("\(data.summ)")
              .font(.system(size: 15))
              .foregroundColor(.white)
              .frame(width: size.width, height: 25, alignment: .center)
          }
        }
        .padding(.bottom, 5)
        .frame(width: size.width, height: 50, alignment: .center)
      }
      .contentShape(Rectangle())
      .frame(width: size.width, height: size.height, alignment: .center)
      .onTapGesture {
        self.tap = true
      }
    }
  }
  init(_ pagination: Int, data: GDCategoryData) {
    
    self.pagination = pagination
    self.data       = data
    self.category   = data.category
  }
}
struct GDCategoryCellPreviews: PreviewProvider {
  static var previews: some View {
    GDCategoryCell(IndexPath.index(0, 0, GDCalculation()), data: GDCategoryData())
      .previewLayout(.fixed(width: 110, height: 160))
  }
}

