
import SwiftUI

struct GDAddCategoryCell: View {
  
  @State var isTapCell = false
  
  var body: some View {
    ZStack{
      Image.set(.addCategory)
      .resizable()
      .frame(width: 100, height: 100)
      .fixedSize()
      .opacity(0.7)
      let addCategoryViewData = AddCategoryViewData()
      let addCategoryView     = AddCategoryView(viewData: addCategoryViewData)
      NavigationLink("", destination: addCategoryView, isActive: $isTapCell)
    }
    .contentShape(Rectangle())
    .onTapGesture {
      self.isTapCell = true
    }
  }
}

struct GDAddCategoryCellPreviews: PreviewProvider {
  static var previews: some View {
    GDAddCategoryCell()
      .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/150.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/150.0/*@END_MENU_TOKEN@*/))
  }
}
