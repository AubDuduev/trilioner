
import SwiftUI

struct GDTransactionCell: View {
  var body: some View {
    GeometryReader() { frame in
      let size = frame.size
      ZStack {
        RoundedRectangle(cornerRadius: 15)
          .foregroundColor(Color(#colorLiteral(red: 0.5275281497, green: 0.9801397895, blue: 1, alpha: 0.5193925062)))
        HStack{
          IconView(viewData: GDCategoryData())
            .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
          Text("Продукты")
            .font(.system(size: 30))
          VStack{
            Text("500")
            Text("14:00")
          }.frame(width: size.height, height: size.height)
        }.frame(width: size.width, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
      }
    }
  }
}

struct GDTransactionCell_Previews: PreviewProvider {
  static var previews: some View {
    GDTransactionCell()
      .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/351.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/))
  }
}
