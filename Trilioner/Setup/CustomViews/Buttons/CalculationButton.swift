
import SwiftUI

struct CalculationButton: View {
  
  public let title            : GDCalculation.GDButton!
  @ObservedObject var viewData: OperationsViewData
  
  var body: some View {
    
    Group {
    Button(action: {
      viewData.name = title.rawValue
    })
    {//Title button
      switch title {
        case .split:
          Text(title.rawValue)
          .font(.system(size: 40))
          .foregroundColor(.white)
        case .ac:
          Text(title.rawValue)
          .font(.system(size: 30))
          .foregroundColor(.white)
        default:
          Text(title.rawValue)
          .font(.system(size: 50))
          .foregroundColor(.white)
      }
    }
   
    }
//    .background(Color.blue)
//    .clipShape(Circle())
  }
}

struct CalculationButtonPreview: PreviewProvider {
  
  static var previews: some View {
    CalculationButton(title: .split, viewData: OperationsViewData())
      .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/))
  }
}

class Testing: ObservableObject {
  
  @Published public var count = "5"
}
