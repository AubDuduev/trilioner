
import SwiftUI

struct IconView: View {
  
  public let viewData: GDCategoryData!
  
  var body: some View {
    GeometryReader{ frame in
      let size = frame.size
      Image(uiImage: viewData.image)
        .resizable()
        .aspectRatio(contentMode: .fit)
        .padding()
        .frame(width: size.width, height: size.width + 5, alignment: .center)
        .background(viewData.category.fon().resizable().scaledToFit())
    }
  }
}

struct IconView_Previews: PreviewProvider {
  static var previews: some View {
    let viewData = GDCategoryData()
    IconView(viewData: viewData)
      .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/))
  }
}
