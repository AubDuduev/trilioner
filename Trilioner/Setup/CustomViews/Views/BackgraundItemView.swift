
import SwiftUI

struct BackgraundItemView: View {
  
  var body: some View {
    
    GeometryReader { frame in
      Image.set(.backgraund)
        .resizable()
        .frame(width: frame.size.width,
               height: frame.size.height)
        .background(Rectangle().foregroundColor(Color(red  : 0,
                                                      green: 0,
                                                      blue : 0).opacity(0.3)))
    }
  }
}
struct BackgraundItemView_Previews: PreviewProvider {
  static var previews: some View {
    BackgraundItemView()
  }
}
