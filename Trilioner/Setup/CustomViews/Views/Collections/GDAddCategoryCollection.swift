
import SwiftUI

struct GDAddCategoryCollection: View {
  
  private let column = 4
  private let row    = (Icon.gains.count / 4)
  
  //MARK: - Public variable ObservedObject
  @ObservedObject public var viewData: AddCategoryViewData
  
  var body: some View {
    GeometryReader() { frame in
      let size = frame.size
      ScrollView(.vertical, showsIndicators: false){
        
        VStack(alignment: .center, spacing: 0) {
          ForEach(0..<row){ row in
            
            HStack(spacing: 0) {
              ForEach(0..<column){ column in
                
                let index = IndexPath.index(row, column, self.column)
                
                Image.set(index: index)
                  .resizable()
                  .frame(width: size.width / 4,
                         height: size.width / 4, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                  .contentShape(Rectangle())
                  .onTapGesture {
                    self.viewData.image = Icon.gains[index]
                  }
                
              }
            }
          }
        }.frame(maxWidth: .infinity)
      }
    }
  }
}

struct GDAddCategoryCollection_Previews: PreviewProvider {
  static var previews: some View {
    let viewData = AddCategoryViewData()
    GDAddCategoryCollection(viewData: viewData)
  }
}
