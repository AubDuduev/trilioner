//
//  Extension + Image.swift
//  Trilioner
//
//  Created by Senior Developer on 28.01.2021.
//

import SwiftUI

extension Image {
  
  static func set(_ name: ImageName) -> Image {
    
    return Image(name.rawValue)
  }
  static func set(index: Int) -> Image {
    let uiimage = Icon.gains[index]
    return Image(uiImage: uiimage)
  }
   
  enum ImageName: String {
    case fonBalance
    case fonIncome
    case fonConsumption
    case fonOperations
    case fonTransactions
    case balanceIcon
    case incomeIcon
    case consumptionIcon
    case operationsIcon
    case transactionsIcon
    case addCategory
    case removeCategory
    case backgraund
  }
}
