
import Foundation

extension IndexPath {
  
  static func index(_ row: Int, _ column: Int, _ collection: Collectionable) -> Int {
    let result = row + column + (row * (collection.column - 1))
  return result
  }
  static func index(_ row: Int, _ column: Int, _ maxColumn: Int) -> Int {
    let result = row + column + (row * (maxColumn - 1))
  return result
  }
  static func lastRow(_ row: Int, _ column: Int,  _ maxColumn: Int,  _ maxRow: Int) -> Bool {
    return IndexPath.index(row, column, maxColumn) == ((maxColumn * maxRow) - 1)
  }
  static func lastRow(_ row: Int, _ column: Int, _ collection: Collectionable) -> Bool {
    return IndexPath.index(row, column, collection) == ((collection.column * collection.row) - 1)
  }
}
